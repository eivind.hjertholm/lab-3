package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String getName() {
        return "echo";
    }

   public String run(String[] args) {

    String returnvalue = ""; 
   
    for (String s : args) {
        returnvalue += s + " ";
    }

    return returnvalue;

    }



    
}
